package eu.weblib.transfert.server;

import android.content.Context;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.text.format.Formatter;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by mael on 11/03/16.
 */
public class Server extends NanoHTTPD {

    private final static String TAG = Server.class.getSimpleName();

    public Server(Context context) throws IOException {
        super(12045);

        WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        String ipAddress = Formatter.formatIpAddress(ip);

        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        Log.d(TAG, "server started, ip is : "+ipAddress);
    }

    @Override
    public Response serve(IHTTPSession session) {

        try {
            if (session.getUri() != null && session.getUri().equals("/conf/tis/currentConf")) {
                String answer = "{\"autolaunchedApps\":[{\"app\":{\"appVersion\":\"1.0\",\"id\":\"104\",\"imageName\":\"logo.png\",\"imageUrl\":\"/apps/104/logo.png\",\"name\":\"screenSaverImage\",\"options\":[{\"condition\":{},\"download\":\"yes\",\"somme\":\"c3e83e00f0c085bcb6b48248e504aabdf85cda8c\",\"type\":\"image\",\"name\":\"androidgreylogo1453393206097.jpg\",\"value\":\"/config/829/apps/24361/androidgreylogo1453393206097.jpg\"},{\"condition\":{},\"download\":\"yes\",\"somme\":\"468c07e4e868ad1b8d08d073807b3d7fb37c139f\",\"type\":\"image\",\"name\":\"minimalisticandroidonabluebackground1453393212840.jpg\",\"value\":\"/config/829/apps/24361/minimalisticandroidonabluebackground1453393212840.jpg\"}],\"type\":\"screenSaverImage\",\"packageUrl\":\"\"},\"position\":\"1\"}],\"name\":\"Mael Test\",\"configRevNumber\":\"829-451\",\"desktops\":[{\"apps\":[{\"app\":{\"appVersion\":\"1.3.4\",\"id\":\"10095\",\"imageName\":\"wikipedia.png\",\"imageUrl\":\"/apps/10095/wikipedia.png\",\"name\":\"Wikipedia\",\"options\":[{\"condition\":{},\"download\":\"no\"}],\"packageName\":\"org.wikipedia\",\"type\":\"app\",\"packageUrl\":\"/apps/10095/org.wikipedia.apk\"},\"position\":\"1\"},{\"app\":{\"appVersion\":\"1.0.4\",\"id\":\"11022\",\"imageName\":\"\",\"imageUrl\":\"\",\"name\":\"uniqlo london\",\"options\":[{\"condition\":{},\"download\":\"no\"}],\"packageName\":\"fr.goandup.uniqlo\",\"type\":\"app\",\"packageUrl\":\"/apps/11022/fr.goandup.uniqlo.apk\"},\"position\":\"2\"},{\"app\":{\"appVersion\":\"1.0.0\",\"id\":\"11023\",\"imageName\":\"\",\"imageUrl\":\"\",\"name\":\"uniqlo weblib\",\"options\":[{\"condition\":{},\"download\":\"no\"}],\"packageName\":\"eu.weblib.uniqlonewsletter\",\"type\":\"app\",\"packageUrl\":\"/apps/11023/eu.weblib.uniqlonewsletter.apk\"},\"position\":\"3\"}],\"screen\":1}],\"hasDesktop\":\"1\",\"password\":{\"admin\":\"a\",\"kioskPinCode\":\"0000\",\"user\":\"a\"},\"screenClosingSession\":{\"landscape\":{\"type\":\"image\",\"url\":\"/config/829/sessionclose/56508c70fe7b40e00b46b0bb58e9ad21large1453386661878.jpeg\",\"name\":\"56508c70fe7b40e00b46b0bb58e9ad21large1453386661878.jpeg\"},\"portrait\":{\"type\":\"image\",\"url\":\"/config/829/sessionclose/totolarge1453386850277.jpg\",\"name\":\"totolarge1453386850277.jpg\"}},\"screenOpeningSession\":{\"portrait\":{\"type\":\"image\",\"url\":\"/config/829/sessionopen/totolarge1453386843925.jpg\",\"name\":\"totolarge1453386843925.jpg\"}},\"sessionTime\":{\"hard\":\"10\",\"soft\":\"5\"},\"standby\":[{\"displayScreenSaver\":\"0\",\"hourStart\":\"10:58 AM\",\"hourEnd\":\"10:59 AM\"}],\"update\":\"04:00 PM\",\"wallpaper\":{\"landscape\":{\"type\":\"image\",\"url\":\"/config/829/wallpapers/56508c70fe7b40e00b46b0bb58e9ad21large1453386656910.jpeg\",\"name\":\"56508c70fe7b40e00b46b0bb58e9ad21large1453386656910.jpeg\"},\"portrait\":{\"type\":\"image\",\"url\":\"/config/829/wallpapers/totolarge1453386847191.jpg\",\"name\":\"totolarge1453386847191.jpg\"}}}";
                return sendJson(Response.Status.OK, answer);
            } else if (session.getUri() != null && session.getUri().equals("/image")) {
                return sendImage(session.getParms().get("file"));
            } else if (session.getUri() != null && session.getUri().equals("/app")) {
                return sendApk(session.getParms().get("file"));
            } else if (session.getUri() != null && session.getUri().equals("/video")) {
                return sendVideo(session.getParms().get("file"));
            }
        }
        catch (Exception e){
            Log.e(TAG, "error : "+e.getMessage());
        }

        return newFixedLengthResponse(Response.Status.NOT_FOUND, "application/json; charset=utf-8", "{\"status\":\"error\",\"message\":\"Bad uri\"}");

    }

    public Response sendJson(Response.IStatus status, String content){
        return newFixedLengthResponse(status, "application/json; charset=utf-8", content);
    }

    public Response sendImage(String image) throws Exception {
        File file = new File(Environment.getExternalStorageDirectory() + "/kiosk/IMAGE/"+image);
        return newChunkedResponse(Response.Status.OK, MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(image)), new FileInputStream(file));
    }

    public Response sendApk(String apk) throws Exception {
        File file = new File(Environment.getExternalStorageDirectory() + "/kiosk/APK/"+apk);
        return newChunkedResponse(Response.Status.OK, "application/vnd.android.package-archive", new FileInputStream(file));
    }

    public Response sendVideo(String video) throws Exception {
        File file = new File(Environment.getExternalStorageDirectory() + "/kiosk/VIDEO/"+video);
        return newChunkedResponse(Response.Status.OK, MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(video)), new FileInputStream(file));
    }

}
