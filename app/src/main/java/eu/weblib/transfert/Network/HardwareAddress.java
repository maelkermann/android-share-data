/*
 * Copyright (C) 2009-2010 Aubort Jean-Baptiste (Rorist)
 * Licensed under GNU's GPL 2, see README
 */

// http://standards.ieee.org/regauth/oui/oui.txt

package eu.weblib.transfert.Network;


import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HardwareAddress extends AsyncTask<Void, Void, String>{

    private final static String TAG = "HardwareAddress";
    private final static String IP_RE = "^(([0-9]{1,3}.){3}.([0-9]{1,3}))\\s+0x1\\s+0x2\\s+(%s+)\\s+\\*\\s+\\w+$";
    private final static int BUF = 8 * 1024;

    private String mMac;
    private OnGetAddress mListener;

    public HardwareAddress(String mac, OnGetAddress listener) {
        this.mMac = mac;
        this.mListener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {
        String ip = getIpAddress();
        if ( ip == null ){
            pingAllDevices();
            ip = getIpAddress();
        }

        return ip;
    }

    @Override
    protected void onPostExecute(String ip){

        if ( ip != null ) {
            mListener.onSuccess(ip);
        }else{
            mListener.onFailure();
        }

    }

    public interface OnGetAddress{
        void onSuccess(String ip);
        void onFailure();
    }

    public String getIpAddress(){

        String hw = null;
        BufferedReader bufferedReader = null;
        try {
            if (mMac != null) {
                String ptrn = String.format(IP_RE, mMac.replace(".", "\\."));
                Pattern pattern = Pattern.compile(ptrn);
                bufferedReader = new BufferedReader(new FileReader("/proc/net/arp"), BUF);
                String line;
                Matcher matcher;
                while ((line = bufferedReader.readLine()) != null) {
                    matcher = pattern.matcher(line);
                    if (matcher.matches()) {
                        hw = matcher.group(1);
                        break;
                    }
                }
            } else {
                Log.e(TAG, "ip is null");
            }
        } catch (IOException e) {
            Log.e(TAG, "Can't open/read file ARP: " + e.getMessage());
            mListener.onFailure();
            return null;
        } finally {
            try {
                if(bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        return hw;

    }

    /**
     * Returns MAC address of the given interface name.
     * @return  mac address or empty string
     */
    private void pingAllDevices() {

        String ip = getLocalIpAddress();

        try {
            NetworkInterface iFace = NetworkInterface.getByInetAddress(InetAddress.getByName(ip));

            for (int i = 0; i <= 255; i++) {

                // build the next IP address
                String addr = ip;
                addr = addr.substring(0, addr.lastIndexOf('.') + 1) + i;
                InetAddress pingAddr = InetAddress.getByName(addr);

                // 50ms Timeout for the "ping"
                if (pingAddr.isReachable(iFace, 200, 50)) {
                    Log.d("PING", pingAddr.getHostAddress());
                }
            }

            getIpAddress();

        } catch (UnknownHostException ex) {
        } catch (IOException ex) {
        }
    }

    /**
     * Get IP address from first non-localhost interface
     * @return  address or empty string
     */
    private String getLocalIpAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;
                        if (isIPv4)
                            return sAddr;
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

}
