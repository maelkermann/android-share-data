package eu.weblib.transfert;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import eu.weblib.transfert.Network.HardwareAddress;
import eu.weblib.transfert.server.Server;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = MainActivity.class.getSimpleName();
    private final static String PORT = "12045";

    private View buttonServer, buttonClient, buttonScan;
    private Server mServer;
    private DownloadManager mDownloadManager = null;
    private DownloadReceiver mDownloadReceiver = null;

    private Map<String, String> urls   = new HashMap<>();
    private Map<Long, String> downloads = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonServer    = findViewById(R.id.buttonServer);
        buttonServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createServer();
            }
        });
        buttonClient    = findViewById(R.id.buttonClient);
        buttonClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIp();
            }
        });
        buttonScan      = findViewById(R.id.buttonScan);
        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findIpFromMac();
            }
        });

        mDownloadManager   = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        mDownloadReceiver  = new DownloadReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        intentFilter.addAction(DownloadManager.ACTION_NOTIFICATION_CLICKED);
        registerReceiver(mDownloadReceiver, intentFilter);

        urls.put("conf", "http://%s:%s/conf/tis/currentConf");
        urls.put("START_LANDSCAPE.png", "http://%s:%s/image?file=START_LANDSCAPE.png");
        urls.put("visuel_newsletter1457111966897.jpg", "http://%s:%s/image?file=visuel_newsletter1457111966897.jpg");
        urls.put("fr.goandup.uniqlo.apk", "http://%s:%s/apk?file=fr.goandup.uniqlo.apk");

    }

    @Override
    public void onPause(){
        super.onPause();

        if ( mServer != null ) {
            mServer.closeAllConnections();
            mServer.stop();
        }
    }


    private void createServer(){
        try {
            mServer = new Server(this);
        } catch (IOException e) {
            Log.e(TAG, "error : "+e.getMessage());
        }

    }

    private void getIp(){

        (new HardwareAddress("b0:47:bf:28:0c:50", new HardwareAddress.OnGetAddress() {
            @Override
            public void onSuccess(String ip) {
                createClient(ip);
            }

            @Override
            public void onFailure() {
                Toast.makeText(MainActivity.this, "Failed to find ip", Toast.LENGTH_SHORT).show();
            }
        })).execute();

    }

    private void createClient(String ip){

        Log.d(TAG, "IP address : "+ip);
        if ( ip == null )
            return;

        for ( String file : urls.keySet() ) {

            String url  = String.format(urls.get(file), ip, PORT);
            Log.d(TAG, "url : "+url);

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setDestinationInExternalPublicDir("", "test/"+file);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
            request.setVisibleInDownloadsUi(false);
            request.setTitle(file);
            long downloadId = mDownloadManager.enqueue(request);
            downloads.put(downloadId, file);
        }

    }

    private void findIpFromMac(){



    }

    private class DownloadReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(TAG, "[DM] " + "DownloadReceiver action is " + action);
            Bundle bun = intent.getExtras();
            if (bun == null) {
                return;
            }
            long downloadid = bun.getLong(DownloadManager.EXTRA_DOWNLOAD_ID);
            if ( downloads.get(downloadid) != null ) {
                Log.d(TAG, "[DM#" + downloadid + "] " + " just finished");
            }
        }
    }

}
